const values = (obj) => {
    const valuesArray = [];

    if (obj != undefined && typeof obj === 'object' && !Array.isArray(obj)) {

        for (const key in obj) {
            if (Object.hasOwn(obj, key) && typeof obj[key] !== 'function') {
                valuesArray.push(obj[key]);
            }

        }
    }

    return valuesArray;
}

module.exports = values;
