const keys = (obj) => {
    const KeysArray = [];

    if (obj != undefined && typeof obj === 'object') {

        for (const key in obj) {
            KeysArray.push(key.toString());
        }
    }

    return KeysArray;
}

module.exports = keys;
