const values = (obj) => {
    const pairArray = [];

    if (obj != undefined && typeof obj === 'object' && !Array.isArray(obj)) {

        for (const key in obj) {

            if (Object.hasOwn(obj, key) && typeof obj[key] !== 'function') {
                pairArray.push([key.toString(), obj[key]]);
            }
        }
    }

    return pairArray;
}

module.exports = values;
