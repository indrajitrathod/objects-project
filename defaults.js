const defaults = (obj, defaultProps) => {
    let filledObj = {};

    if (obj != undefined && typeof obj === 'object' && !Array.isArray(obj)) {
        filledObj = obj;

        if (defaultProps != undefined && typeof defaultProps === 'object' && !Array.isArray(defaultProps)) {

            for (const key in defaultProps) {

                if (obj[key] === undefined) {
                    obj[key] = defaultProps[key];
                }

            }
        }
    }

    return filledObj;
}

module.exports = defaults;
