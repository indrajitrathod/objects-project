const invert = (obj) => {
    const invertObj = {};

    if (obj != undefined && typeof obj === 'object' && !Array.isArray(obj)) {

        for (const key in obj) {
            if (Object.hasOwn(obj, key) && typeof obj[key] !== 'function') {
                invertObj[obj[key].toString()] = key.toString();
            }

        }
    }

    return invertObj;
}

module.exports = invert;
