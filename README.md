# Objects Project

### A **simple project on objects**, which does few operations on Objects.

## This project creates 6 functions

* #### Function 1
    ```
        function keys(obj) {

        // Retrieve all the names of the object's properties.
        // Return the keys as strings in an array.
        // Based on http://underscorejs.org/#keys
    }
    ```

* #### Function 2
    ```
        function values(obj) {

        // Return all of the values of the object's own properties.
        // Ignore functions
        // http://underscorejs.org/#values
    }
    ```

* #### Function 3
    ```
        function mapObject(obj, cb) {

        // Like map for arrays, but for objects. Transform the value of each property in turn by passing it to the callback function.
        // http://underscorejs.org/#mapObject
    }
    ```
* #### Function 4
    ```
        function pairs(obj) {

        // Convert an object into a list of [key, value] pairs.
        // http://underscorejs.org/#pairs
    }
    ```

* #### Function 5
    ```
        function invert(obj) {

        // Returns a copy of the object where the keys have become the values and the values the keys.
        // Assume that all of the object's values will be unique and string serializable.
        // http://underscorejs.org/#invert
    }
    ```

* #### Function 6
    ```
        function defaults(obj, defaultProps) {

        // Fill in undefined properties that match properties on the `defaultProps` parameter object.
        // Return `obj`.
        // http://underscorejs.org/#defaults
    }
    ```
