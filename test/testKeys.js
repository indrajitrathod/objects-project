const { describe } = require('mocha');
const asset = require('chai');

const keys = require('../keys');
const { assert } = require('chai');
const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

describe('keys.js', () => {
    it('Function shouldn\'t throw error and return empty array when no object is passed', () => {
        const result = keys();

        assert.isArray(result);
        assert.isEmpty(result);
    });

    it('Function shouldn\'t throw error and return empty array when argument is empty object', () => {
        const result = keys({});

        assert.isArray(result);
        assert.isEmpty(result);
    });

    it('Function should return expected result when arguement is a proper object', () => {
        const result = keys(testObject);

        assert.deepEqual(result, ['name', 'age', 'location']);

    });

    it('Function should return empty array when argument is not an object', () => {
        const result = keys(true);
        const result2 = keys(false);
        const result3 = keys(null);
        const result4 = keys(1);
        const result5 = keys(2.345);
        const result6 = keys('Mountblue');
        const result7 = keys('% ');
        const result8 = keys([]);
        const result9 = keys('{}');

        assert.isArray(result);
        assert.isEmpty(result);
        assert.isArray(result2);
        assert.isEmpty(result2);
        assert.isArray(result3);
        assert.isEmpty(result3);
        assert.isArray(result4);
        assert.isEmpty(result4);
        assert.isArray(result5);
        assert.isEmpty(result5);
        assert.isArray(result6);
        assert.isEmpty(result6);
        assert.isArray(result7);
        assert.isEmpty(result7);
        assert.isArray(result8);
        assert.isEmpty(result8);
        assert.isArray(result9);
        assert.isEmpty(result9);
        // assert.isArray(result10);
        // assert.isEmpty(result10);
    });
});