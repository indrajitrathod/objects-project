const defaults = require('../defaults');
const { assert } = require('chai');

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
const defaultProps = { location: 'Gotham', age: 36 };

describe('defaults.js', () => {
    it('Function shouldn\'t throw error and return empty object when no object is passed', () => {
        const result = defaults();

        assert.isObject(result);
        assert.isEmpty(result);
        assert.deepEqual(result, {});

    });

    it('Function shouldn\'t throw error and return original object when no defaultProps is passed', () => {
        const result = defaults(testObject);

        assert.isObject(result, testObject);
    });

    it('Function should return expected result when arguement is a proper object', () => {
        const testObject = {
            name: 'Bruce Wayne',
            age: 36
        };

        const expected = {
            name: 'Bruce Wayne',
            age: 36,
            location: 'Gotham'
        };

        const result = defaults(testObject, defaultProps);
        assert.deepEqual(result, expected);

    });

    it('Function should return empty Object when argument is not an object', () => {
        const result = defaults(true);
        const result2 = defaults(false);
        const result3 = defaults(null);
        const result4 = defaults(1);
        const result5 = defaults(2.345);
        const result6 = defaults('Mountblue');
        const result7 = defaults('% ');
        const result8 = defaults([]);
        const result9 = defaults('{}');
        const result10 = defaults([{}]);

        assert.isObject(result);
        assert.isEmpty(result);
        assert.isObject(result2);
        assert.isEmpty(result2);
        assert.isObject(result3);
        assert.isEmpty(result3);
        assert.isObject(result4);
        assert.isEmpty(result4);
        assert.isObject(result5);
        assert.isEmpty(result5);
        assert.isObject(result6);
        assert.isEmpty(result6);
        assert.isObject(result7);
        assert.isEmpty(result7);
        assert.isObject(result8);
        assert.isEmpty(result8);
        assert.isObject(result9);
        assert.isEmpty(result9);
        assert.isObject(result10);
        assert.isEmpty(result10);
    });
});