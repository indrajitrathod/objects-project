const { describe } = require('mocha');
const asset = require('chai');

const values = require('../values');
const { assert } = require('chai');
const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

describe('values.js', () => {
    it('Function shouldn\'t throw error and return empty array when no object is passed', () => {
        const result = values();

        assert.isArray(result);
        assert.isEmpty(result);
    });

    it('Function shouldn\'t throw error and return empty array when argument is empty object', () => {
        const result = values({});

        assert.isArray(result);
        assert.isEmpty(result);
    });

    it('Function should return expected result when arguement is a proper object', () => {
        const result = values(testObject);

        assert.deepEqual(result, ['Bruce Wayne', 36, 'Gotham']);
    });

    it('Function should return only properties not functions', () => {
        const testObject = { 
            name: 'Bruce Wayne',
            age: 36, 
            location: 'Gotham', 
            myFunction(){
                console.log(`I am the Batman`);
            },
            myCity: ()=>{
                return location;
            }
         };
        const result = values(testObject);

        assert.deepEqual(result, ['Bruce Wayne', 36, 'Gotham']);
    });

    it('Function should return only Object\'s own properties', () => {
        const baseObject = { company_name: 'Mountblue', course: 'JavaScript' };
        const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

        const result = values(testObject);

        assert.deepEqual(result, ['Bruce Wayne', 36, 'Gotham']);
    });

    it('Function should return empty array when argument is not an object', () => {
        const result = values(true);
        const result2 = values(false);
        const result3 = values(null);
        const result4 = values(1);
        const result5 = values(2.345);
        const result6 = values('Mountblue');
        const result7 = values('% ');
        const result8 = values([]);
        const result9 = values('{}');
        const result10 = values([{}]);

        assert.isArray(result);
        assert.isEmpty(result);
        assert.isArray(result2);
        assert.isEmpty(result2);
        assert.isArray(result3);
        assert.isEmpty(result3);
        assert.isArray(result4);
        assert.isEmpty(result4);
        assert.isArray(result5);
        assert.isEmpty(result5);
        assert.isArray(result6);
        assert.isEmpty(result6);
        assert.isArray(result7);
        assert.isEmpty(result7);
        assert.isArray(result8);
        assert.isEmpty(result8);
        assert.isArray(result9);
        assert.isEmpty(result9);
        assert.isArray(result10);
        assert.isEmpty(result10);
    });
});