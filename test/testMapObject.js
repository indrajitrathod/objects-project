const { describe } = require('mocha');
const asset = require('chai');

const mapObject = require('../mapObject');
const { assert } = require('chai');
const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

describe('mapObject.js', () => {
    it('Function shouldn\'t throw error and return empty array when no object is passed', () => {
        const result = mapObject();

        assert.isArray(result);
        assert.isEmpty(result);
    });

    it('Function shouldn\'t throw error and return empty array when argument is empty object', () => {
        const result = mapObject({});

        assert.isArray(result);
        assert.isEmpty(result);
    });

    it('Function should return expected result when arguements are correct', () => {
        const result = mapObject(testObject, (value) => {
            return value + ' Mountblue';
        });

        assert.deepEqual(result, ['Bruce Wayne Mountblue', '36 Mountblue', 'Gotham Mountblue']);
    });

    it('Function should return empty array when callback function argument is not a function', () => {
        const result = mapObject(testObject, 123);
        const result2 = mapObject(testObject, []);
        const result3 = mapObject(testObject, true);
        const result4 = mapObject(testObject, {});
        const result5 = mapObject(testObject, 'Hello');

        assert.isArray(result);
        assert.isEmpty(result);
        assert.isArray(result2);
        assert.isEmpty(result2);
        assert.isArray(result3);
        assert.isEmpty(result3);
        assert.isArray(result4);
        assert.isEmpty(result4);
        assert.isArray(result5);

    });

    it('Function should return empty array when argument is not an object', () => {
        const result = mapObject(true);
        const result2 = mapObject(false);
        const result3 = mapObject(null);
        const result4 = mapObject(1);
        const result5 = mapObject(2.345);
        const result6 = mapObject('Mountblue');
        const result7 = mapObject('% ');
        const result8 = mapObject([]);
        const result9 = mapObject('{}');
        const result10 = mapObject([{}]);

        assert.isArray(result);
        assert.isEmpty(result);
        assert.isArray(result2);
        assert.isEmpty(result2);
        assert.isArray(result3);
        assert.isEmpty(result3);
        assert.isArray(result4);
        assert.isEmpty(result4);
        assert.isArray(result5);
        assert.isEmpty(result5);
        assert.isArray(result6);
        assert.isEmpty(result6);
        assert.isArray(result7);
        assert.isEmpty(result7);
        assert.isArray(result8);
        assert.isEmpty(result8);
        assert.isArray(result9);
        assert.isEmpty(result9);
        assert.isArray(result10);
        assert.isEmpty(result10);
    });
});