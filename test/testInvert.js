const { describe } = require('mocha');
const asset = require('chai');

const invert = require('../invert');
const { assert } = require('chai');
const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

describe('invert.js', () => {
    it('Function shouldn\'t throw error and return empty array when no object is passed', () => {
        const result = invert();

        assert.isObject(result);
        assert.isEmpty(result);
    });

    it('Function shouldn\'t throw error and return empty array when argument is empty object', () => {
        const result = invert({});

        assert.isObject(result);
        assert.isEmpty(result);
    });

    it('Function should return expected result when arguement is a proper object', () => {
        const result = invert(testObject);
        const expected = {
            'Bruce Wayne': 'name',
            '36': 'age',
            'Gotham': 'location'
        };

        assert.deepEqual(result, expected);
    });

    it('Function should return only properties not functions', () => {
        const testObject = {
            name: 'Bruce Wayne',
            age: 36,
            location: 'Gotham',
            myFunction() {
                console.log(`I am the Batman`);
            },
            myCity: () => {
                return location;
            }
        };
        const result = invert(testObject);
        const expected = {
            'Bruce Wayne': 'name',
            '36': 'age',
            'Gotham': 'location'
        };

        assert.deepEqual(result, expected);
    });

    it('Function should return empty object when argument is not an object', () => {
        const result = invert(true);
        const result2 = invert(false);
        const result3 = invert(null);
        const result4 = invert(1);
        const result5 = invert(2.345);
        const result6 = invert('Mountblue');
        const result7 = invert('% ');
        const result8 = invert([]);
        const result9 = invert('{}');
        const result10 = invert([{}]);

        assert.isObject(result);
        assert.isEmpty(result);
        assert.isObject(result2);
        assert.isEmpty(result2);
        assert.isObject(result3);
        assert.isEmpty(result3);
        assert.isObject(result4);
        assert.isEmpty(result4);
        assert.isObject(result5);
        assert.isEmpty(result5);
        assert.isObject(result6);
        assert.isEmpty(result6);
        assert.isObject(result7);
        assert.isEmpty(result7);
        assert.isObject(result8);
        assert.isEmpty(result8);
        assert.isObject(result9);
        assert.isEmpty(result9);
        assert.isObject(result10);
        assert.isEmpty(result10);
    });
});