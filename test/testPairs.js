const { describe } = require('mocha');
const asset = require('chai');

const pairs = require('../pairs');
const { assert } = require('chai');
const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

describe('pairs.js', () => {
    it('Function shouldn\'t throw error and return empty array when no object is passed', () => {
        const result = pairs();

        assert.isArray(result);
        assert.isEmpty(result);
    });

    it('Function shouldn\'t throw error and return empty array when argument is empty object', () => {
        const result = pairs({});

        assert.isArray(result);
        assert.isEmpty(result);
    });

    it('Function should return expected result when arguement is a proper object', () => {
        const result = pairs(testObject);

        assert.deepEqual(result, [['name', 'Bruce Wayne'], ['age', 36], ['location', 'Gotham']]);
    });

    it('Function should return only properties not functions', () => {
        const testObject = {
            name: 'Bruce Wayne',
            age: 36,
            location: 'Gotham',
            myFunction() {
                console.log(`I am the Batman`);
            },
            myCity: () => {
                return location;
            }
        };
        const result = pairs(testObject);

        assert.deepEqual(result, [['name', 'Bruce Wayne'], ['age', 36], ['location', 'Gotham']]);
    });

    it('Function should return empty array when argument is not an object', () => {
        const result = pairs(true);
        const result2 = pairs(false);
        const result3 = pairs(null);
        const result4 = pairs(1);
        const result5 = pairs(2.345);
        const result6 = pairs('Mountblue');
        const result7 = pairs('% ');
        const result8 = pairs([]);
        const result9 = pairs('{}');
        const result10 = pairs([{}]);

        assert.isArray(result);
        assert.isEmpty(result);
        assert.isArray(result2);
        assert.isEmpty(result2);
        assert.isArray(result3);
        assert.isEmpty(result3);
        assert.isArray(result4);
        assert.isEmpty(result4);
        assert.isArray(result5);
        assert.isEmpty(result5);
        assert.isArray(result6);
        assert.isEmpty(result6);
        assert.isArray(result7);
        assert.isEmpty(result7);
        assert.isArray(result8);
        assert.isEmpty(result8);
        assert.isArray(result9);
        assert.isEmpty(result9);
        assert.isArray(result10);
        assert.isEmpty(result10);
    });
});