const mapObject = (obj, cb) => {
    const mappedArray = [];

    if (obj != undefined && cb != undefined && typeof obj === 'object' && !Array.isArray(obj) && typeof cb === 'function') {

        for (const key in obj) {

            mappedArray.push(cb(obj[key]));
        }

    }

    return mappedArray;
}

module.exports = mapObject;
